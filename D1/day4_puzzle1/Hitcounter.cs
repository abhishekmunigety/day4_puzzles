﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day4_puzzle1
{
   	class Hitcounter
		{

			internal List<int> L = new List<int>();

			//records a hit that happened at timestamp
			public virtual void Record(int TimeStamp)
			{
				L.Add(TimeStamp);
			}


			// to Return the Total number of Hits recorded
			public virtual int Total()
			{
				return L.Count;
			}


			// returns the number of hits that occurred between timestamps lower and upper(inclusive)
			public virtual int Range(int start, int end)
			{
				int count = 0;
				for (int i = start; i <= end; i++)
				{
					count++;
				}
				return count;
			}


			public static void Main(string[] args)
			{

				Hitcounter HC = new Hitcounter();
				HC.Record(10);
				HC.Record(12);
				HC.Record(14);
				HC.Record(16);
				HC.Record(18);
				HC.Record(24);

				Console.WriteLine("the total number of hits are : " + HC.Total());
				Console.WriteLine("number of hits that occurred between timestamps lower and upper (inclusive) : " + HC.Range(14, 24));
			}

		}
	}
    