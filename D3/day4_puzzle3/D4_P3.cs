﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day4_puzzle3
{
public class Node
        {
            public int data;
            public Node L = null;
            public Node R = null;
            public Node(int data)
            {
                this.data = data;
            }
        }
        class D4_P3
        {
            public static bool printPath(Node root, int sum)
            {
                if (sum == 0 && root == null)
                    return true;
                if (root == null)
                {
                    return false;
                }
                bool left = printPath(root.L, sum - root.data);
                bool right = false;
                if (!left)
                    right = printPath(root.R, sum - root.data);
                if (left || right)
                    Console.WriteLine(root.data);
                return left || right;
            }
            public static int Minpath(Node root)
            {
                if (root == null)
                    return int.MinValue;
                if (root.L == null && root.R == null)
                    return root.data;
                int left = Minpath(root.L);
                int right = Minpath(root.R);

                return (left > right ? left : right) + root.data;
            }
            public static void MaxSum(Node root)
            {
                int sum = Minpath(root);
                Console.WriteLine(sum);

            }
            static void Main(string[] args)
            {
                Node root = new Node(3);
                root.L = new Node(2);
                root.R = new Node(1);
                root.L.L = new Node(1); 
            Console.WriteLine("the max sum from the root of the node is");
                MaxSum( root);
            }
        }

    }

